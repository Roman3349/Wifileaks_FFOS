$(document).ready(function() {
  localStorage.setItem("scan", true);
  scanWiFi();
  $("#scan").click(function() {
    scanWiFi();
    console.info(time + ' [SCAN] run scan');
  });
  $("#stop").click(function() {
    localStorage.setItem("scan", false);
    console.info(time + ' [SCAN] stop scan');
  })
});

var time = (new Date.getTime()) / 1000;

var GPSOptions = {
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
};

function scanWiFi() {
  if (localStorage.getItem("scan")) {
    if (checkSensors()) {
      navigator.geolocation.getCurrentPosition(save, error, GPSOptions);
    }
    var delay = localStorage.getItem("delay");
    pause(delay);
    scanWiFi();
  }
}

function checkSensors() {
  if (!navigator.geolocation) {
    alert("Vaše zařízení nemá zapnutou GPS!");
    console.info(time + ' [SCAN] GPS is disabled');
    return false;
  } else if (!navigator.mozWifiManager.enabled) {
    alert("Vaše zařízení nemá zapnutou WiFi!");
    console.info(time + ' [SCAN] WiFi is disabled');
    return false;
  } else {
    return true;
  }
}

function error(e) {
  console.warn(time + ' [SCAN] ERROR(' + e.code + '): ' + e.message);
}

function save(GPS) {
  /* GPS */
  var coords = GPS.coords;
  var latitude = coords.latitude;
  var longitude = coords.longitude;
  var altitude = coords.altitude;
  var accuracy = coords.accuracy;
  /* WiFi */
  var wifi = navigator.mozWifiManager.getNetworks();
  wifi.onsuccess = function () {
    var networks = this.result;
    site.forEach(function (network) {
      var ssid = network.ssid;
      var mac = network.bssid;
      var sec = network.security;
      var signal = network.signal;
      $("#ssid").html(ssid);
      $("#mac").html(mac);
      $("#sec").html(sec);
      $("#signal").html(signal + " dBm");
      $("#latitude").html(latitude);
      $("#longitude").html(longitude);
      $("#fix").html(fix);
    });
  }
}