function login(username, password) {
  var data = new FormData();
  data.append("username", username);
  data.append("password", password);
  var request = new XMLHttpRequest();
  var url = "http://auth.wifileaks.cz/";
  request.open("POST", url, true);
  request.send(data);
}

function upload(file, token) {
  var data = new FormData();
  data.append("wifileaksfile", file);
  data.append("token", token);
  data.append("atoken", "");
  data.append("version", "");
  data.append("manufacturer", "");
  data.append("model", "");
  var request = new XMLHttpRequest();
  var url = "http://postdb.wifileaks.cz/";
  request.open("POST", url, true);
  request.send(data);
}